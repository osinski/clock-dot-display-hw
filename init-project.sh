#! /bin/bash

set -e

VALID_ARGS=$(getopt -o a:t:h --long author:,title:,help -- "$@")

if [[ $? -ne 0 ]]; then
    exit 1;
fi

AUTHOR=$(unset)
TITLE=$(unset)

printHelp()
{
    echo "Usage: $(basename "$0") [-a | --author 'AUTHOR']"
    echo "                        [-t | --title TITLE]"
    echo "Title shouldn't contain whitespaces. Use '-' to separate words."
    echo "Both arguments need to be set!"
}

eval set -- "$VALID_ARGS"
while  :  
do
    case "$1" in
        -a | --author)
            echo "Setting project's author to '$2'"
            AUTHOR="$2"
            shift 2
            ;;
        -t | --title)
            echo "Setting project's title to '$2'"
            TITLE="$2"
            shift 2
            ;;
        -h | --help)
            echo "Initialize KiCAD Project."
            printHelp
            exit 0
            ;;
        --)
            shift;
            break
            ;;
    esac
done

if [[ -z $AUTHOR ]]
then
    echo "Missing author's name!"
    printHelp
    exit 1
fi

if [[ $(echo "$TITLE" | wc -w) -ne 1 ]]
then
    echo "Missing project's title or it's more than one word!"
    printHelp
    exit 1
fi

echo "Initializing library submodule"
cd ./kicad-library || (echo "Cannot cd to library directory!"; exit 1)
git checkout master
git pull
git checkout -b "$TITLE"
cd - || (echo "Cannot return to parent directory!"; exit 1)

echo "Updating Makefile with project name"
sed -i "s/<<PROJECT_NAME>>/$TITLE/" ./Makefile

echo "Renaming files and substituting project's author and title"
sed -i "s/<<author>>/$AUTHOR/" \
        kicad-template-project.kicad_sch
sed -i "s/<<author>>/$AUTHOR/" \
        kicad-template-project.kicad_pcb
sed -i "s/<<kicad-template-project>>/$TITLE/" \
        kicad-template-project.kicad_sch
sed -i "s/<<kicad-template-project>>/$TITLE/" \
        kicad-template-project.kicad_pcb
sed -i "s/kicad-template-project\.kicad_pro/$TITLE.kicad_pro/" \
        kicad-template-project.kicad_pro
sed -i "s/kicad-template-project\.kicad_prl/$TITLE.kicad_prl/" \
        kicad-template-project.kicad_prl
mv kicad-template-project.kicad_sch "$TITLE".kicad_sch
mv kicad-template-project.kicad_pcb "$TITLE".kicad_pcb
mv kicad-template-project.kicad_prl "$TITLE".kicad_prl
mv kicad-template-project.kicad_pro "$TITLE".kicad_pro


SYMBOL_LIBS=$(find ./kicad-library -name '*.kicad_sym')
FOOTPRINT_LIBS=$(find ./kicad-library -type d -name '*.pretty')

echo "Populating sym-lib-table"
echo "(sym_lib_table" > ./sym-lib-table
for LIB_PATH in $SYMBOL_LIBS
do
    LIB_NAME=$(echo -e "$LIB_PATH" | sed -e 's/\.\/kicad-library\/\(.*\)\.kicad_sym/\1/')
    echo -e "Adding: $LIB_NAME"
    echo -e "  (lib (name \"$LIB_NAME\")(type \"KiCad\")(uri \"\${KIPRJMOD}/kicad-library/$LIB_NAME.kicad_sym\")(options \"\")(descr \"\"))" >> ./sym-lib-table
done
echo ")" >> ./sym-lib-table

echo "Populating fp-lib-table"
echo "(fp_lib_table" > ./fp-lib-table
for LIB_PATH in $FOOTPRINT_LIBS
do
    LIB_NAME=$(echo -e "$LIB_PATH" | sed -e 's/\.\/kicad-library\/\(.*\)\.pretty/\1/')
    echo -e "Adding: $LIB_NAME"
    echo -e "  (lib (name \"$LIB_NAME\")(type \"KiCad\")(uri \"\${KIPRJMOD}/kicad-library/$LIB_NAME.pretty\")(options \"\")(descr \"\"))" >> ./fp-lib-table
done
echo ")" >> ./fp-lib-table

echo "Creating additional directories"
mkdir plots-imgs
mkdir mechanical

echo "Creating pre-commit git hook"
mv pre-commit-hook .git/hooks/pre-commit

echo "Removing note in README.md saying it is template repository"
sed -i '3d' ./README.md

echo "All done!"
