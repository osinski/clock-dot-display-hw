# Marcin Osiński 2022-2024

PROJECT_NAME = Clock-Dot-Display
MAIN_SCH_FILE = $(PROJECT_NAME).kicad_sch
SCH_FILES = $(shell find ./ -name '*.kicad_sch')
PCB_FILE = $(PROJECT_NAME).kicad_pcb

MFG_FILES_DIR = ./outputs
PLOT_FILES_DIR = ./plots-imgs


all: erc drc bom interactive-bom assembly-outputs schematics 3d-model layer-plots fabrication-outputs


fabrication-outputs:
	@echo "--- Generating Board Fabrication Outputs ---"
	@kicad-cli pcb export gerbers --exclude-value --subtract-soldermask\
		--layers F.Cu,In1.Cu,In2.Cu,B.Cu,F.Mask,B.Mask,F.Paste,B.Paste,F.Silkscreen,B.Silkscreen,Edge.Cuts\
		$(PCB_FILE) -o $(MFG_FILES_DIR) > /dev/null
	@kicad-cli pcb export drill \
		--format excellon --drill-origin absolute --excellon-zeros-format decimal \
		--excellon-units mm --excellon-separate-th --generate-map --map-format gerberx2 \
		$(PCB_FILE) -o $(MFG_FILES_DIR) > /dev/null
	@echo "--- Zipping them up! ---"
	@zip -r $(MFG_FILES_DIR)/gerbers.zip \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-F_Cu.gtl \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-In1_Cu.g2 \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-In2_Cu.g3 \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-B_Cu.gbl \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-F_Mask.gts \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-B_Mask.gbs \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-F_Paste.gtp \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-B_Paste.gbp \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-F_Silkscreen.gto \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-B_Silkscreen.gbo \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-Edge_Cuts.gm1 \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-NPTH-drl_map.gbr \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-NPTH.drl \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-PTH-drl_map.gbr \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-PTH.drl \
		> /dev/null
	@rm $(MFG_FILES_DIR)/$(PROJECT_NAME)-*.g[btm23]* $(MFG_FILES_DIR)/*.drl
	@echo "--- Done! ---"


assembly-outputs: jlcpcb-assembly


interactive-bom: $(PCB_FILE)
	@echo "--- Generating Interactive BOM file in $(MFG_FILES_DIR) ---"
	@kicad-cli sch export netlist -o $(PROJECT_NAME).net $(MAIN_SCH_FILE) > /dev/null
	@generate_interactive_bom \
		--dark-mode \
		--netlist-file $(PROJECT_NAME).net \
		--extra-fields "Mfr,MPN,Summary" \
		--dest-dir $(MFG_FILES_DIR) \
		--name-format interactive-bom \
		--no-browser \
		$(PCB_FILE) > /dev/null
	@rm $(PROJECT_NAME).net
	@echo "--- Done! ---"


pick-n-place: $(PCB_FILE)
	@echo "--- Generating Pick n Place Files in $(MFG_FILES_DIR) ---"
	@kicad-cli pcb export pos --format csv --units mm --side both \
		--smd-only --exclude-dnp \
		-o $(MFG_FILES_DIR)/$(PROJECT_NAME)-pos.csv $(PCB_FILE) > /dev/null
	@echo "--- Done! ---"

jlcpcb-assembly: pick-n-place bom
	@echo "--- Generating JLCPCB specific BoM and P&P files ---"
	@awk -F, 'NR == 1 {print "Designator, Mid X, Mid Y, Layer, Rotation"; next} \
		{gsub(/"/, "", $$1); print $$1", "$$4", "$$5", "$$7", "$$6}' \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-pos.csv > \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-jlc-pos.csv
	@awk -F';' 'NR == 1 {print "Comment,Designator,Footprint,JLCPCB Part"; next} \
		{comment = $$3 " " $$4 " " $$5; \
		gsub(/,/, " ", $$1); \
		gsub(/,/, "", comment); \
		gsub(/.*:/, "", $$6); \
		print comment","$$1","$$6","$$7}' \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-BoM.csv > \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-jlc-BoM.csv
	#@cp $(MFG_FILES_DIR)/$(PROJECT_NAME)-jlc-BoM.csv \
	#	$(MFG_FILES_DIR)/$(PROJECT_NAME)-jlc-BoM-manual.csv
	#@cp $(MFG_FILES_DIR)/$(PROJECT_NAME)-jlc-pos.csv \
	#	$(MFG_FILES_DIR)/$(PROJECT_NAME)-jlc-pos-manual.csv
	@echo "--- Done! ---"


schematics: $(SCH_FILES)
	@echo "--- Generating PDF Schematics in $(PLOT_FILES_DIR) ---"
	@kicad-cli sch export pdf --black-and-white \
		--output $(PLOT_FILES_DIR)/schematic-prints.pdf $(PROJECT_NAME).kicad_sch \
		> /dev/null
	@echo "--- Done! ---"


bom: $(SCH_FILES)
	@echo "--- Generating Bill of Materials in $(MFG_FILES_DIR) ---"
	@kicad-cli sch export bom --exclude-dnp \
				--string-delimiter "" --ref-delimiter "," --field-delimiter ";"\
				--fields 'Reference,$${QUANTITY},Mfr,MPN,Summary,Footprint,JLCPCB Part' \
				--labels 'Ref,Qty,Mfr,MPN,Summary,Footprint,JLCPCB Part' \
				-o $(MFG_FILES_DIR)/$(PROJECT_NAME)-BoM.csv Clock-Dot-Display.kicad_sch \
				> /dev/null
	@echo "--- Done! ---"


layer-plots: $(PCB_FILE)
	@echo "--- Generating PCB Layer Plots in $(PLOT_FILES_DIR) ---"
	@kicad-cli pcb export pdf --ibt --black-and-white \
		--layers Edge.Cuts,User.Drawings,F.Cu \
		-o $(PLOT_FILES_DIR)/pcb-layers-top.pdf \
		$(PCB_FILE) > /dev/null
	@kicad-cli pcb export pdf --ibt --black-and-white \
		--layers Edge.Cuts,In1.Cu \
		-o $(PLOT_FILES_DIR)/pcb-layers-in1.pdf \
		$(PCB_FILE) > /dev/null
	@kicad-cli pcb export pdf --ibt --black-and-white \
		--layers Edge.Cuts,In2.Cu \
		-o $(PLOT_FILES_DIR)/pcb-layers-in2.pdf \
		$(PCB_FILE) > /dev/null
	@kicad-cli pcb export pdf --ibt --black-and-white \
		--layers Edge.Cuts,B.Cu \
		-o $(PLOT_FILES_DIR)/pcb-layers-bottom.pdf \
		$(PCB_FILE) > /dev/null
	@kicad-cli pcb export pdf --ibt --black-and-white \
		--layers Edge.Cuts,User.Drawings,F.Silkscreen \
		-o $(PLOT_FILES_DIR)/pcb-layers-silk-top.pdf \
		$(PCB_FILE) > /dev/null
	@kicad-cli pcb export pdf --ibt --black-and-white --mirror \
		--layers Edge.Cuts,B.Silkscreen \
		-o $(PLOT_FILES_DIR)/pcb-layers-silk-bottom.pdf \
		$(PCB_FILE) > /dev/null
	@kicad-cli pcb export pdf --ibt --black-and-white \
		--layers Edge.Cuts,User.Drawings,F.Fab \
		-o $(PLOT_FILES_DIR)/pcb-layers-assembly-top.pdf \
		$(PCB_FILE) > /dev/null
	@kicad-cli pcb export pdf --ibt --black-and-white --mirror \
		--layers Edge.Cuts,B.Fab \
		-o $(PLOT_FILES_DIR)/pcb-layers-assembly-bottom.pdf \
		$(PCB_FILE) > /dev/null
	@gs -dQUIET \
		-dBATCH \
		-dNOPAUSE \
		-dAutoRotatePages=/None \
		-sDEVICE=pdfwrite \
		-sOutputFile=$(PLOT_FILES_DIR)/pcb-layers.pdf \
		$(PLOT_FILES_DIR)/pcb-layers-top.pdf \
		$(PLOT_FILES_DIR)/pcb-layers-in1.pdf \
		$(PLOT_FILES_DIR)/pcb-layers-in2.pdf \
		$(PLOT_FILES_DIR)/pcb-layers-bottom.pdf \
		$(PLOT_FILES_DIR)/pcb-layers-silk-top.pdf \
		$(PLOT_FILES_DIR)/pcb-layers-silk-bottom.pdf \
		$(PLOT_FILES_DIR)/pcb-layers-assembly-top.pdf \
		$(PLOT_FILES_DIR)/pcb-layers-assembly-bottom.pdf
	@rm $(PLOT_FILES_DIR)/pcb-layers-*.pdf
	@echo "--- Done! ---"


pcb-drawings: $(PCB_FILE)
	#note pcbdraw has problems working
	@echo "--- Generating PCB Drawings in $(PLOT_FILES_DIR) ---"
	@pcbdraw plot --side front --silent --style $(PLOT_FILES_DIR)/pcbdraw-style.json \
		$(PCB_FILE) $(PLOT_FILES_DIR)/board-top.png
	@pcbdraw plot --side back --silent --style $(PLOT_FILES_DIR)/pcbdraw-style.json \
		$(PCB_FILE) $(PLOT_FILES_DIR)/board-back.png

	@echo "--- Done! ---"


3d-model: $(PCB_FILE)
	@echo "--- Generating basic 3D Model of PCB in $(MFG_FILES_DIR) ---"
	@kicad-cli pcb export step --grid-origin --force --no-dnp \
		--output $(MFG_FILES_DIR)/step-model-basic.step $(PCB_FILE) \
		> /dev/null
	@sed -i -e's/[[:space:]]*$$//' $(MFG_FILES_DIR)/step-model-basic.step
	@echo "--- Done! ---"


erc: $(SCH_FILES)
	@echo "--- Running Electrical Rules Check ---"
	@kicad-cli sch erc --exit-code-violations \
		--output $(MFG_FILES_DIR)/erc_results.txt $(MAIN_SCH_FILE) \
		> /dev/null || echo "ERC not clean!"
	@echo "--- Done! ---"


drc: $(PCB_FILE)
	@echo "--- Running Design Rules Check ---"
	@kicad-cli pcb drc --severity-error --severity-warning --exit-code-violations \
		--output $(MFG_FILES_DIR)/drc_results.txt $(PCB_FILE) \
		> /dev/null || echo "DRC not clean!"
	@echo "--- Done! ---"


clean:
	@echo "--- Removing Generated Files ---"
	@find $(MFG_FILES_DIR) -mindepth 1 \
		! -name '*-manual.csv' \
		-exec rm -f {} +
	@find $(PLOT_FILES_DIR) -mindepth 1 \
		! -name 'board-top.png' ! -name 'board-back.png' \
		-exec rm -f {} +

	@echo "--- Done! ---"

