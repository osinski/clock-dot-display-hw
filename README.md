# *Template* Kicad Project Repository


## Preview screenshots

<p align="center">
	<img src="./plots-imgs/board-top.png"  width="40%"  />
	<img src="./plots-imgs/board-back.png" width="40%"  />
</p>

## About

*<<write project description>>*


## Output files

*Gerber* and *Drill* (prepared according to **JLCPCB** specification) files are
zipped together and reside in `outputs` directory. *CSV* *BOM* file,
*interactive BOM* *html* file and *ERC* and *DRC* reports are also in there.

Schematic and PCB layer prints, as well as some preview images reside in
`plots-imgs` directory.


